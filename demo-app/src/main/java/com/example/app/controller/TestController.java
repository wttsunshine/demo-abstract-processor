package com.example.app.controller;

import com.example.component.entity.BlyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author Benjamin
 * @Date 2021/12/4
 */
@RestController
@RequestMapping("/bly")
public class TestController {

	private BlyEntity blyEntity;

	@Autowired
	public void setBlyEntity(BlyEntity blyEntity) {
		this.blyEntity = blyEntity;
	}

	@GetMapping("test")
	public String test(){
		return blyEntity.getName();
	}
}
