package com.example.component.entity;

/**
 * @Description TODO
 * @Author Benjamin
 * @Date 2021/12/4
 */
public class BlyEntity {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BlyEntity{" +
			"name='" + name + '\'' +
			'}';
	}
}
